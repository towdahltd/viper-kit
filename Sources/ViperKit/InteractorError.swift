import Foundation

public protocol InteractorError: Error {
    var title: String { get }
    var content: String { get }
}
