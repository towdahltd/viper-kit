import SwiftUI
import Combine

open class Presenter<CustomInteractor: Interactor> {
  public var interactor: CustomInteractor
  
  public var cancellables = Set<AnyCancellable>()
  public var routerCancellable: AnyCancellable? = nil
  @Published public var isLoading: Bool = false
  @Published public var errorAlert: AlertItem?
  
  public init(interactor: CustomInteractor) {
    self.interactor = interactor

    interactor.$error
      .filter { $0 != nil }
      .compactMap { AlertItem(title: Text($0!.title), message: Text($0!.content)) }
      .assign(to: \.errorAlert, on: self)
      .store(in: &cancellables)
    
    interactor.$isLoading
      .assign(to: \.isLoading, on: self)
      .store(in: &cancellables)
  }
}
