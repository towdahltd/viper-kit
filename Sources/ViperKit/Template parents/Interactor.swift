import Foundation
import Combine

open class Interactor {
  @Published public var error: InteractorError?
  @Published public var isLoading: Bool = false
  
  public var cancellables = Set<AnyCancellable>()
  
  public init() { }
}
