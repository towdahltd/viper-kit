import SwiftUI
import Combine

open class RouterHandler: ObservableObject {
  struct State {
    var navigating: AnyView? = nil
    var presentingSheet: AnyView? = nil
    var isPresented: Binding<Bool>
  }
  
  @Published var state: State
  
  public init(isPresented: Binding<Bool>) {
    state = State(isPresented: isPresented)
  }
}

extension RouterHandler {
  
  func navigateTo<V: View>(_ view: V) {
    state.navigating = AnyView(view)
  }
  
  func presentSheet<V: View>(_ view: V) {
    state.presentingSheet = AnyView(view)
  }
  
  func dismiss() {
    state.isPresented.wrappedValue = false
  }
}

extension RouterHandler {
  
  public var isNavigating: Binding<Bool> {
    boolBinding(keyPath: \.navigating)
  }
  
  public var isPresentingSheet: Binding<Bool> {
    boolBinding(keyPath: \.presentingSheet)
  }
  
  public var isPresented: Binding<Bool> {
    state.isPresented
  }
}

private extension RouterHandler {
  
  func binding<T>(keyPath: WritableKeyPath<State, T>) -> Binding<T> {
    Binding(
      get: { self.state[keyPath: keyPath] },
      set: { self.state[keyPath: keyPath] = $0 }
    )
  }
  
  func boolBinding<T>(keyPath: WritableKeyPath<State, T?>) -> Binding<Bool> {
    Binding(
      get: { self.state[keyPath: keyPath] != nil },
      set: {
        if !$0 {
          self.state[keyPath: keyPath] = nil
        }
      }
    )
  }
}

extension View {
  public func navigation(_ router: RouterHandler) -> some View {
    self.modifier(NavigationModifier(presentingView: router.binding(keyPath: \.navigating)))
  }
  
  public func sheet(_ router: RouterHandler) -> some View {
    self.modifier(SheetModifier(presentingView: router.binding(keyPath: \.presentingSheet)))
  }
}
