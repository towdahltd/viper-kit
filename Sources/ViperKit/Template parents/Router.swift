import SwiftUI

open class Router: RouterHandler {
  override public init(isPresented: Binding<Bool>) {
    super.init(isPresented: isPresented)
  }
}

public protocol Routable where Self: RouterHandler {
  associatedtype D
  associatedtype V: View
  func view(for destination: D) -> V
}

extension Routable {
  public func navigate(to destination: D) {
    navigateTo(view(for: destination))
  }
  
  public func present(_ destination: D) {
    presentSheet(view(for: destination))
  }
}
