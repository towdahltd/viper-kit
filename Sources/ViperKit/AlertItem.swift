import SwiftUI

public struct AlertItem: Identifiable {
  public var id = UUID()
  public var title: Text
  public var message: Text?
  public var dismissButton: Alert.Button?
  
  public init(title: Text, message: Text? = nil, dismissButton: Alert.Button? = nil) {
    self.title = title
    self.message = message
    self.dismissButton = dismissButton
  }
}
