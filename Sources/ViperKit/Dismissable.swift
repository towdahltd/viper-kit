import SwiftUI

public protocol Dismissable where Self: View {
  var presentationMode: Binding<PresentationMode> { get }

  func dismiss()
}

extension Dismissable {
  public func dismiss() {
    self.presentationMode.wrappedValue.dismiss()
  }
}
